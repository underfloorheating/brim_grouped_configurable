/**
 * Brim LLC Commercial Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Brim LLC Commercial Extension License
 * that is bundled with this package in the file license.pdf.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.brimllc.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@brimllc.com so we can send you a copy immediately.
 *
 * @category   Brim
 * @package    Brim_Groupedoptions
 * @copyright  Copyright (c) 2011-2016 Brim LLC
 * @license    http://ecommerce.brimllc.com/license
 */

var GroupedOptions = {
    simple : {},
    configurable : {}
};

Product.OptionsPrice.prototype.reload = function() {
    var price;
    var formattedPrice;
    var optionPrices = this.getOptionPrices();
    var nonTaxable = optionPrices[1];
    var optionOldPrice = optionPrices[2];
    var priceInclTax = optionPrices[3];
    optionPrices = optionPrices[0];

    $H(this.containers).each(function(pair) {
        var _productPrice;
        var _plusDisposition;
        var _minusDisposition;
        var _priceInclTax;
        if ($(pair.value)) {
            if (pair.value == 'old-price-'+this.productId && this.productOldPrice != this.productPrice) {
                _productPrice = this.productOldPrice;
                _plusDisposition = this.oldPlusDisposition;
                _minusDisposition = this.oldMinusDisposition;
            } else {
                _productPrice = this.productPrice;
                _plusDisposition = this.plusDisposition;
                _minusDisposition = this.minusDisposition;
            }
            _priceInclTax = priceInclTax;

            if (pair.value == 'old-price-'+this.productId && optionOldPrice !== undefined) {
                price = optionOldPrice+parseFloat(_productPrice);
            } else if (this.specialTaxPrice == 'true' && this.priceInclTax !== undefined && this.priceExclTax !== undefined) {
                price = optionPrices+parseFloat(this.priceExclTax);
                _priceInclTax += this.priceInclTax;
            } else {
                price = optionPrices+parseFloat(_productPrice);
                _priceInclTax += parseFloat(_productPrice) * (100 + this.currentTax) / 100;
            }

            if (this.specialTaxPrice == 'true') {
                var excl = price;
                var incl = _priceInclTax;
            } else if (this.includeTax == 'true') {
                // tax = tax included into product price by admin
                var tax = price / (100 + this.defaultTax) * this.defaultTax;
                var excl = price - tax;
                var incl = excl*(1+(this.currentTax/100));
            } else {
                var tax = price * (this.currentTax / 100);
                var excl = price;
                var incl = excl + tax;
            }

            var subPrice = 0;
            var subPriceincludeTax = 0;
            Object.values(this.customPrices).each(function(el){
                if (el.excludeTax && el.includeTax) {
                    subPrice += parseFloat(el.excludeTax);
                    subPriceincludeTax += parseFloat(el.includeTax);
                } else {
                    subPrice += parseFloat(el.price);
                    subPriceincludeTax += parseFloat(el.price);
                }
            });
            excl += subPrice;
            incl += subPriceincludeTax;

            if (typeof this.exclDisposition == 'undefined') {
                excl += parseFloat(_plusDisposition);
            }

            incl += parseFloat(_plusDisposition) + parseFloat(this.plusDispositionTax);
            excl -= parseFloat(_minusDisposition);
            incl -= parseFloat(_minusDisposition);

            //adding nontaxlable part of options
            excl += parseFloat(nonTaxable);
            incl += parseFloat(nonTaxable);

            if (pair.value == 'price-including-tax-'+this.productId) {
                price = incl;
            } else if (pair.value == 'price-excluding-tax-'+this.productId) {
                price = excl;
            } else if (pair.value == 'old-price-'+this.productId) {
                if (this.showIncludeTax || this.showBothPrices) {
                    price = incl;
                } else {
                    price = excl;
                }
            } else {
                if (this.showIncludeTax) {
                    price = incl;
                } else {
                    price = excl;
                }
            }

            if (price < 0) price = 0;

            if (price > 0 || this.displayZeroPrice) {
                formattedPrice = this.formatPrice(price);
            } else {
                formattedPrice = '';
            }

            if ($(pair.value).select('.price')[0]) {
                $(pair.value).select('.price')[0].innerHTML = formattedPrice;
                if ($(pair.value+this.duplicateIdSuffix) && $(pair.value+this.duplicateIdSuffix).select('.price')[0]) {
                    $(pair.value+this.duplicateIdSuffix).select('.price')[0].innerHTML = formattedPrice;
                }
            } else {
                $(pair.value).innerHTML = formattedPrice;
                if ($(pair.value+this.duplicateIdSuffix)) {
                    $(pair.value+this.duplicateIdSuffix).innerHTML = formattedPrice;
                }
            }
        };
    }.bind(this));

    if (typeof(skipTierPricePercentUpdate) === "undefined" && typeof(this.tierPrices) !== "undefined") {
        for (var i = 0; i < this.tierPrices.length; i++) {
            $$('.grouped-product-row-' + this.productId + ' .benefit').each(function (el) {
                var parsePrice = function (html) {
                    var format = this.priceFormat;
                    var decimalSymbol = format.decimalSymbol === undefined ? "," : format.decimalSymbol;
                    var regexStr = '[^0-9-' + decimalSymbol + ']';
                    //remove all characters except number and decimal symbol
                    html = html.replace(new RegExp(regexStr, 'g'), '');
                    html = html.replace(decimalSymbol, '.');
                    return parseFloat(html);
                }.bind(this);

                var updateTierPriceInfo = function (priceEl, tierPriceDiff, tierPriceEl, benefitEl) {
                    if (typeof(tierPriceEl) === "undefined") {
                        //tierPrice is not shown, e.g., MAP, no need to update the tier price info
                        return;
                    }
                    var price = parsePrice(priceEl.innerHTML);
                    var tierPrice = price + tierPriceDiff;

                    tierPriceEl.innerHTML = this.formatPrice(tierPrice);

                    var $percent = Selector.findChildElements(benefitEl, ['.percent.tier-' + i]);
                    $percent.each(function (el) {
                        el.innerHTML = Math.ceil(100 - ((100 / price) * tierPrice));
                    });
                }.bind(this);

                var tierPriceElArray = $$('.grouped-product-row-' + this.productId + ' .tier-price.tier-' + i + ' .price');
                if (this.showBothPrices) {
                    var containerExclTax = $(this.containers[3]);
                    var tierPriceExclTaxDiff = this.tierPrices[i];
                    var tierPriceExclTaxEl = tierPriceElArray[0];
                    updateTierPriceInfo(containerExclTax, tierPriceExclTaxDiff, tierPriceExclTaxEl, el);
                    var containerInclTax = $(this.containers[2]);
                    var tierPriceInclTaxDiff = this.tierPricesInclTax[i];
                    var tierPriceInclTaxEl = tierPriceElArray[1];
                    updateTierPriceInfo(containerInclTax, tierPriceInclTaxDiff, tierPriceInclTaxEl, el);
                } else if (this.showIncludeTax) {
                    var container = $(this.containers[0]);
                    var tierPriceInclTaxDiff = this.tierPricesInclTax[i];
                    var tierPriceInclTaxEl = tierPriceElArray[0];
                    updateTierPriceInfo(container, tierPriceInclTaxDiff, tierPriceInclTaxEl, el);
                } else {
                    var container = $(this.containers[0]);
                    var tierPriceExclTaxDiff = this.tierPrices[i];
                    var tierPriceExclTaxEl = tierPriceElArray[0];
                    updateTierPriceInfo(container, tierPriceExclTaxDiff, tierPriceExclTaxEl, el);
                }
            }, this);
        }
    }
};

// Modified version of Product.Options found in options.phtml
// This class handles multiple products on a single page.
Product.GroupedSimpleOptions = Class.create();
Product.GroupedSimpleOptions.prototype = {
    initialize : function(config, optionsPrice, optionsClass){
        this.config = config;
        this.optionsPrice = optionsPrice;
        this.optionsClass = optionsClass;
        this.reloadPrice();

        // register self with the global object
        GroupedOptions.simple[optionsPrice.productId] = this;
    },
    reloadPrice : function(){
        var object = this;
        var price = new Number();
        var config = this.config;
        skipIds = [];
        $$('.'+this.optionsClass).each(function(element){
            var optionId = 0;
            element.name.sub(/\[([0-9]+)\]\[([0-9]+)\]/, function(match){
                optionId = match[2]; // expects the name to be in the form super_options[product_id][option_id]
            });
            if (optionId == 0) {
                var result = /options_(\d+)_file/gi.exec(element.name);
                if (result && result[1]) {
                    optionId = result[1];
                }
            }

            if (config[optionId]) {
                if (element.type == 'checkbox' || element.type == 'radio') {
                    if (element.checked) {
                        if (typeof config[optionId][element.getValue()] != 'undefined') {
                            price += object._getOptionPrice(config[optionId][element.getValue()]);
                        }
                    }
                } else if(element.hasClassName('datetime-picker') && !skipIds.include(optionId)) {
                    dateSelected = true;
                    $$('.product-custom-option[id^="superoptions_' + optionId + '"]').each(function(dt){
                        if (dt.getValue() == '') {
                            dateSelected = false;
                        }
                    });
                    if (dateSelected) {
                        price += object._getOptionPrice(config[optionId]);
                        skipIds[optionId] = optionId;
                    }
                } else if(element.type == 'select-one' || element.type == 'select-multiple') {
                    if (element.options) {
                        $A(element.options).each(function(selectOption){
                            if (selectOption.selected) {
                                if (typeof config[optionId][selectOption.value] != 'undefined') {
                                    price += object._getOptionPrice(config[optionId][selectOption.value]);
                                }
                            }
                        });
                    }
                } else {
                    if (element.getValue().strip() != '') {
                        price += object._getOptionPrice(config[optionId]);
                    }
                }
            }
        });
        try {
            var productId = this.optionsPrice.productId;
            if (typeof GroupedOptions.configurable[productId] != 'undefined') {
                // cross set pricing changes.
                var confOptionsPrice = GroupedOptions.configurable[productId].optionsPrice;
                if (typeof confOptionsPrice.optionPrices['config'] != 'undefined') {
                    this.optionsPrice.changePrice('config', confOptionsPrice.optionPrices['config']);
                }
                confOptionsPrice.changePrice('options', price);
            }

            this.optionsPrice.changePrice('options', price);
            this.optionsPrice.reload();
        } catch (e) {
        }
    },
    _getOptionPrice: function(optionConfig) {
        var price = 0;
        if (typeof optionConfig == 'object') {
            price += parseFloat(optionConfig.price);
        } else {
            price += parseFloat(optionConfig);
        }
        return price;
    }
}

Product.Config.prototype.loadOptions = function() {
    this.settings.each(function(element){
        element.disabled = false;
        element.options[0] = new Option(this.config.chooseText, '');
        var attributeId = element.id.replace(/[a-z]*/, '');
        var options = this.getAttributeOptions(attributeId);
        if(options) {
            var index = 1;
            for(var i=0;i<options.length;i++){
                options[i].allowedProducts = options[i].products.clone();
                element.options[index] = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                if (typeof options[i].price != 'undefined') {
                    element.options[index].setAttribute('price', options[i].price);
                }
                element.options[index].setAttribute('data-label', options[i].label.toLowerCase());
                element.options[index].config = options[i];
                index++;
            }
        }
        this.reloadOptionLabels(element);
    }.bind(this));
}


// This new js class extends the Product.Config class to allow
// us to specify a optionsPrice. This allows us to maintain multiple
// instances of this class on a single page.
Product.GroupedConfig = Class.create(Product.Config, {
    initialize: function($super, config, productId){
        config.containerId = 'product_addtocart_wrapper_' + productId;
        $super(config);

        this.config     = config;
        this.taxConfig  = this.config.taxConfig;
        this.productId  = productId;
        this.settings   = $$('#product_addtocart_wrapper_' + productId + ' .super-attribute-select');
        this.state      = new Hash();
        this.priceTemplate = new Template(this.config.template);
        this.prices     = config.prices;

        this.settings.each(function(element){
            Event.observe(element, 'change', this.configure.bind(this))
        }.bind(this));

        // register self with the global object
        GroupedOptions.configurable[productId] = this;

        // fill state
        this.settings.each(function(element){
            var attributeId = element.id.replace(/[a-z]*/, '');
            if(attributeId && this.config.attributes[attributeId]) {
                element.config = this.config.attributes[attributeId];
                element.attributeId = attributeId;
                this.state[attributeId] = false;
            }
        }.bind(this))

        // Init settings dropdown
        var childSettings = [];
        for(var i=this.settings.length-1;i>=0;i--){
            var prevSetting = this.settings[i-1] ? this.settings[i-1] : false;
            var nextSetting = this.settings[i+1] ? this.settings[i+1] : false;
            if(i==0){
                this.fillSelect(this.settings[i])
            }
            else {
                this.settings[i].disabled=true;
            }
            $(this.settings[i]).childSettings = childSettings.clone();
            $(this.settings[i]).prevSetting   = prevSetting;
            $(this.settings[i]).nextSetting   = nextSetting;
            childSettings.push(this.settings[i]);
        }

        // try retireve options from url
        var separatorIndex = window.location.href.indexOf('#');
        if (separatorIndex!=-1) {
            var paramsStr = window.location.href.substr(separatorIndex+1);
            this.values = paramsStr.toQueryParams();
            this.settings.each(function(element){
                var attributeId = element.attributeId;
                element.value = (typeof(this.values[attributeId]) == 'undefined')? '' : this.values[attributeId];
                this.configureElement(element);
            }.bind(this));
        }
    },
    setOptionsPrice: function(optionsPrice){
        this.optionsPrice = optionsPrice;
    },
    reloadPrice: function($super) {
        optionsPrice = this.optionsPrice;
        var price = $super.call(this);

        var productId = this.optionsPrice.productId;
        if (typeof GroupedOptions.simple[productId] != 'undefined') {
            // cross set pricing changes.
            var simpleOptionsPrice =  GroupedOptions.simple[productId].optionsPrice;
            if (typeof simpleOptionsPrice.optionPrices['options'] != 'undefined') {
                this.optionsPrice.changePrice('options', simpleOptionsPrice.optionPrices['options']);
            }
            simpleOptionsPrice.changePrice('config', price);
        }

        return price;
    }
});

Validation.addAllThese([['go-qty-required-entry', 'This is a required field.', function(v, elm) {
    try {
        var result = /[a-z_-]+\[(\d+)\]\[\d+\]/gi.exec(elm.name) || /go-product-(\d+)/gi.exec(elm.readAttribute('class'));
        if (result != null && result[1]) {
            var qtyElm = $$('[name="super_group[' + result[1] + ']"]').first();
            if (qtyElm.value > 0) {
                return !Validation.get('IsEmpty').test(v);
            }
        }
    } catch(e) { console.log(e); }

    return true;
}],
    ['go-qty-one-required', 'Please select one.', function(v, elm){
        try {
            var result = /[a-z_-]+\[(\d+)\]\[\d+\]/gi.exec(elm.name);
            if (result[1]) {
                var qtyElm = $$('[name="super_group[' + result[1] + ']"]').first();
                if (qtyElm.value > 0) {
                    var p = elm.parentNode.parentNode;
                    var options = p.getElementsByTagName('INPUT');
                    return $A(options).any(function(elm) {
                        return $F(elm);
                    });
                }
            }
        } catch(e) { console.log(e); }

        return true;
    }]
]);

GroupedOptions.ConfigurableSwatchesDefault_E = Object.toJSON(Product.ConfigurableSwatches.prototype._E);

Product.ConfigurableSwatches.prototype.initialize = Product.ConfigurableSwatches.prototype.initialize.wrap(function(callOriginal, productConfig, config) {
    // moved into the instance to prevent shared configurable options.
    this._E = GroupedOptions.ConfigurableSwatchesDefault_E.evalJSON();

    callOriginal(productConfig, config);

    $$('.configurable-swatch-list-' + productConfig.config.productId + ' a.swatch-link').each(function(element) {
        Event.observe(element, 'click', function(event) {
            var qtyField = $$('input[name="super_group['+productConfig.config.productId+']"]').first();
            if (qtyField.value == 0) { qtyField.value = 1 }
        });
    });

    return this;
});

/**

/* Adding swatch support */
Product.ConfigurableSwatches.prototype.setAttrData = function(attr, i) {
    var productId = this.productConfig.config.productId;

    var attributeId = 'attribute';
    for (var i = 0; i < productId.length; i++) {
        attributeId += String.fromCharCode( parseInt(productId.charAt(i))+97);
    }
    attributeId += attr.id;

    var optionSelect = $(attributeId);
    // Flags
    attr._f = {};
    // FIXME for Custom Option Support
    attr._f.isCustomOption = false;
    attr._f.isSwatch = optionSelect.hasClassName('swatch-select');
    // Elements
    attr._e = {
        optionSelect: optionSelect,
        attrLabel: this._u.getAttrLabelElement( attr.code, productId ),
        selectedOption: false,
        _last: {
            selectedOption: false
        }
    };
    attr._e.optionSelect.attr = attr;
    if (attr._f.isSwatch) {
        attr._e.ul = $('configurable_swatch_' + productId + '_' + attr.code);
    };
    return attr;
}

Product.ConfigurableSwatches.prototype._u.getAttrLabelElement = Product.ConfigurableSwatches.prototype._u.getAttrLabelElement.wrap(function(callOriginal, attrCode, productId) {
    attrCode = productId + '_' + attrCode;

    return callOriginal(attrCode);
});

Product.ConfigurableSwatches.prototype.setOptData = function(opt, attr, j) {

    var productId = this.productConfig.config.productId;

    // Store Attribute on option
    opt.attr = attr;
    // Flags
    opt._f = {
        isSwatch: attr._f.isSwatch,
        enabled: true,
        active: false
    };
    // Elements
    opt._e = {
        option: this._u.getOptionElement(opt, attr, j)
    };
    opt._e.option.opt = opt;
    if (attr._f.isSwatch) {
        opt._e.a = $('swatch'+productId+ '_' + opt.id);
        opt._e.li = $('option'+productId + '_' + opt.id);
        opt._e.ul = attr._e.ul;
    }
    return opt;
};